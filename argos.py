from bs4 import BeautifulSoup
import requests
import mysql.connector
from itertools import chain
import pymysql
from sqlalchemy import *


def argos(db_details):
    cursor = db_details.cursor() #cursor to make queries to the server
    user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36' #useragents 
    headers = {'User-Agent': user_agent} #headers

    url = "https://www.argos.ie/static/Browse/c_1/1|category_root|Office%2C+PCs+and+phones|14418968/c_2/2|14418968|Laptops|50000564/p/1/pp/20/s/Price%3A+Low+-+High.htm" #argos url to be scraped
    response= requests.get(url) #getting the response from the url above and placing it in a variable
    soup = BeautifulSoup(response.content,"html.parser") #calling BS parser for the content called from response

    f_laptop_images = [] #lists for storing values grabbed below
    f_laptop_titles = [] 
    f_laptop_prices = []


    def laptop_grab():
        laptop_titles= soup.findAll("h4", {"class":"description"}) #finding xpath for title
        laptop_prices= soup.findAll('li', attrs={'class':'price'}) #finding xpath for prices 
        laptop_images= soup.findAll('img', attrs={'class':'searchProductImgList'}) #finding xpath link for images
        
        for laptop_image in laptop_images:
            strip = (laptop_image['src'])
            f_laptop_images.append(strip)
            print(strip)
            
        for laptop_title in laptop_titles:
            title = (laptop_title.text.strip())
            strip = title.replace('...more detail', '')
            f_laptop_titles.append(strip)
            print(strip)
        
        for laptop_price in laptop_prices:
            price = (laptop_price.text.strip())
            strip = price.replace(' ', '')
            f_laptop_prices.append(strip)
            print(strip)

    link_exists = True

    while link_exists: #while loop for pagination
        page_divs = soup.findAll('div', {"class":"nextpage"})
        if len(page_divs) == 0:
            break
        first_div = page_divs[0]
        next_link = (first_div.find('a')['href'])
        response= requests.get(next_link)
        soup = BeautifulSoup(response.content,"html.parser")
        laptop_grab() #returns each page contents
    
    all_products = list(chain(*[f_laptop_titles,f_laptop_prices,f_laptop_images])) #chained together to have all products in one list for sqlalchemy
    print(all_products)






    
    
    
    

    
    
