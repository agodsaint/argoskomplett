from bs4 import BeautifulSoup
import requests
import mysql.connector
from itertools import chain
import pymysql
from sqlalchemy import *

user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
headers = {'User-Agent': user_agent}


url = "https://www.komplett.ie/laptops/laptops/"
response= requests.get(url)
soup = BeautifulSoup(response.content,"html.parser")

k_laptop_images = []
k_laptop_titles = []
k_laptop_prices = []

komplett_titles = soup.findAll('div', attrs={'class': 'col-sm-7 productitem-container'})
komplett_prices = soup.findAll('div', attrs={'class': 'productlist-huidigeprijs'})
komplett_images = soup.findAll('div', attrs={'class': 'col-sm-3 productitem-image'})

def laptop_grab():

    komplett_titles = soup.findAll('div', attrs={'class': 'col-sm-7 productitem-container'})
    komplett_prices = soup.findAll('div', attrs={'class': 'productlist-huidigeprijs'})
    komplett_images = soup.findAll('div', attrs={'class': 'col-sm-3 productitem-image'})
    
    for title in komplett_titles:
        span = title.find('span', attrs={'itemprop': 'name'})
        title = span['title']
        k_laptop_titles.append(title)
        print(title)

    for prices in komplett_prices:
        price = (prices.text.strip())
        finprice = price.replace('.-', '')
        k_laptop_prices.append(finprice)
        print(finprice)

    for images in komplett_images:
        imageclass = images.find('img', attrs={'class': 'lazy img-responsive'})
        image = imageclass['data-original']
        k_laptop_images.append(image)
        print(image)

laptop_grab()

for i in range(2,6):   
    url = "https://www.komplett.ie/laptops/laptops/page/{}".format(i)
    r = requests.get(url)
    soup = BeautifulSoup(r.content, "html.parser")
    laptop_grab()


#data = { 'Laptop Title': k_laptop_titles, 'Laptop Price': k_laptop_prices, 'Image Link': k_laptop_images}
#df = DataFrame(data, columns = ['Laptop Title','Laptop Price','Image Link'])
#df.to_csv(r'C:\Users\Sandy\Desktop\anothertestdata.csv')